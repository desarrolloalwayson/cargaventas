DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `spVentasPorSeguro3dias`()
BEGIN
SELECT concat(year(resumenventaFecVigenciaInicialOperacion),'-',month(resumenventaFecVigenciaInicialOperacion),'-',day(resumenventaFecVigenciaInicialOperacion))
					as fecha,
			count(
				CASE resumenventadescSubProducto
					when 'Vida Con Devolución Clásico Internet' THEN 1
                    when 'Vida Con Devolución Clásico Internet 1' THEN 1
					else null
				END
            )
			as 'Vida Con Devolución',
            count(
				CASE resumenventadescSubProducto
					when 'PROTECCION INTEGRAL TARJETA INTERNET' THEN 1
					else null
				END
            )
			as 'Proteccion Integral Tarjeta',
            count(
				CASE resumenventadescSubProducto
					when 'Proteccion Tradicional' THEN 1
					else null
				END
            )
			as 'Proteccion Tradicional',
            count(
				CASE resumenventadescSubProducto
					when 'Proteccion Tradicional Full Internet' THEN 1
					else null
				END
            )
			as 'Protección Tradicional Internet Full',
            count(
				CASE resumenventadescSubProducto
					when 'Proteccion Preferente Full Internet' THEN 1
					else null
				END
            )
			as 'Proteccion Preferente Full Internet',
			count(
				CASE resumenventadescSubProducto
					when 'INCENDIO Y ROBO INTERNET' THEN 1
                    when 'Hogar Contenido' THEN 1
					else null
				END
            )
			as 'Hogar Contenido',
			count(
				CASE resumenventadescSubProducto
					when 'Renta Hospitalaria Internet' THEN 1
					else null
				END
            )
			as 'Renta Hospitalaria',
			count(
				CASE resumenventadescSubProducto
					when 'Bike' THEN 1
					else null
				END
            )
			as 'Bike',
			count(
				CASE resumenventadescSubProducto
					when 'Vida Digital' THEN 1
					else null
				END
            )
			as 'Vida Digital',
			count(
				CASE resumenventadescSubProducto
					when 'Emergencia Accidental' THEN 1
					else null
				END
            )
			as 'Emergencia Accidental',
			count(
				CASE resumenventadescSubProducto
					when 'Seguro Mascota' THEN 1
					else null
				END
            )
			as 'Mascota',
			count(
				CASE resumenventadescSubProducto
					when 'Seguro Cardiologico Internet' THEN 1
					else null
				END
            )
			as 'Cardiológico',
			count(
				CASE resumenventadescSubProducto
					when 'Viaje Protegido' THEN 1
                    when 'Viaje Protegido Plus' THEN 1
					else null
				END
            )
			as 'Viaje Protegido',
			count(
				CASE resumenventadescSubProducto
					when BINARY 'EGRA' THEN 1
                    when 'Egra' THEN 1
					else null
				END
            )
			as 'Egra Internet',
			count(
				CASE resumenventadescSubProducto
					when 'Linea Protegida' THEN 1
					else null
				END
            )
			as 'Linea Protegida',
			count(
				CASE resumenventadescSubProducto
					when 'Asistencia Premium' THEN 1
					else null
				END
            )
			as 'Asistencia Premium',
			count(
				CASE resumenventadescSubProducto
					when 'Vida Educacion' THEN 1
					else null
				END
            )
			as 'Vida Educacion',
			count(
				CASE resumenventadescSubProducto
					when 'Oncologico' THEN 1
					else null
				END
            )
			as 'Oncologico',
			count(
				CASE resumenventadescSubProducto
					when 'Automotriz' THEN 1
					else null
				END
            )
			as 'Automotriz',
            count(*) as 'totaldia'
            from resumenventa
           
				where ResumenVentaFecVigenciaInicialOperacion between
				cast((select date_sub(max(ResumenVentaFecVigenciaInicialOperacion) ,interval 2 day) 
				from resumenventa )as DATE)
				and cast((select max(ResumenVentaFecVigenciaInicialOperacion) 
                from resumenventa ) as DATE)
			group by
			year(resumenventaFecVigenciaInicialOperacion)
			,month(resumenventaFecVigenciaInicialOperacion)
            ,day(resumenventaFecVigenciaInicialOperacion)
			order by
			year(resumenventaFecVigenciaInicialOperacion) desc
			,month(resumenventaFecVigenciaInicialOperacion) desc
            ,day(resumenventaFecVigenciaInicialOperacion);
END$$
DELIMITER ;
