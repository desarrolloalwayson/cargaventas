<!doctype html>
<html lang="en">
  	<head>
    	<!-- Required meta tags -->
	    <meta charset="utf-8">
	    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	</head>
	<body>
		
	

		<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

		<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/jq-3.3.1/jszip-2.5.0/dt-1.10.18/b-1.5.4/b-colvis-1.5.4/b-flash-1.5.4/b-html5-1.5.4/fc-3.2.5/kt-2.5.0/r-2.2.2/rg-1.1.0/datatables.min.css"/>
	 
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
		<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/jq-3.3.1/jszip-2.5.0/dt-1.10.18/b-1.5.4/b-colvis-1.5.4/b-flash-1.5.4/b-html5-1.5.4/fc-3.2.5/kt-2.5.0/r-2.2.2/rg-1.1.0/datatables.min.js"></script>



	
<div class="container-fluid">
<?PHP 
	//require 'vendor/autoload.php';
	// use PhpOffice\PhpSpreadsheet\Spreadsheet;
	// use PhpOffice\PhpSpreadsheet\Reader\Xlsx;


	$servername = "localhost";
	$username = "root";
	$password = "";
	$database = "seguros";

	$conn = new mysqli($servername, $username, $password,$database);

	// Check connection
	if ($conn->connect_error) {
	    die("Connection failed: " . $conn->connect_error);
	} 

	
	$sql = "CALL `seguros`.`spVentasPorSeguro`()";

	// $to = "<table class=".'"table table-dark"'. ">";
	// $tc = "</table>";

	// $theado="<thead class=".'"thead-light"'.">";
	// $theadc="</thead>";

	// $tho="<th>";
	// $thc="</th>";

	// $tro="<tr>";
	// $trc="</tr>";

	// $tdo="<td>";
	// $tdc="</td>";

	// $tbo ="<tbody>";
	// $tbc ="</tbody>";

	// $table = "";

	if($resultado = $conn->query($sql))
	{
		$head = "";
		$anio = "";
		$mes =	"";

		$array_def = array();
		$array_dif = array();
		// $array_seg = array();

		// echo "<pre>";
		// echo var_dump($resultado);
		// echo "</pre>";

		// array_push($array_seg, "Vida Con Devolución Clásico Internet" 	=> 0);
		// array_push($array_seg, "Proteccion Integral Tarjeta Internet" 	=> 0);
		// array_push($array_seg, "Proteccion Tradicional"				    => 0);
		// array_push($array_seg, "Proteccion Tradicional Full Internet"	=> 0);
		// array_push($array_seg, "Proteccion Preferente Full Internet"		=> 0);
		// array_push($array_seg, "Hogar Contenido" 						=> 0);
		// array_push($array_seg, "Renta Hospitalaria Internet" 			=> 0);
		// array_push($array_seg, "Bike" 									=> 0);
		// array_push($array_seg, "Vida Digital" 							=> 0);
		// array_push($array_seg, "Emergencia Accidental" 					=> 0);
		// array_push($array_seg, "Seguro Mascota" 							=> 0);
		// array_push($array_seg, "Seguro Cardiologico Internet" 			=> 0);
		// array_push($array_seg, "Viaje Protegido" 						=> 0);
		// array_push($array_seg, "Egra" 									=> 0);
		// array_push($array_seg, "Linea Protegida" 						=> 0);
		// array_push($array_seg, "Asistencia Premium" 						=> 0);
		// array_push($array_seg, "Vida Educacion" 							=> 0);
		// array_push($array_seg, "Oncologico" 								=> 0);
		// array_push($array_seg, "Automotriz" 								=> 0);
		// 
		$array_seg += 	[	
						"Vida Con Devolución Clásico Internet" 	=> 0,
						"Vida Con Devolución Clásico Internet" 	=> 0,
						"Proteccion Integral Tarjeta Internet" 	=> 0,
						"Proteccion Tradicional"				=> 0,
						"Proteccion Tradicional Full Internet"	=> 0,
						"Proteccion Preferente Full Internet"	=> 0,
						"Hogar Contenido" 						=> 0,
						"Renta Hospitalaria Internet" 			=> 0,
						"Bike" 									=> 0,
						"Vida Digital" 							=> 0,
						"Emergencia Accidental" 				=> 0,
						"Seguro Mascota" 						=> 0,
						"Seguro Cardiologico Internet" 			=> 0,
						"Viaje Protegido Plus" 					=> 0,
						"Egra" 									=> 0,
						"Linea Protegida" 						=> 0,
						"Asistencia Premium" 					=> 0,
						"Vida Educacion" 						=> 0,
						"Oncologico" 							=> 0,
						"Automotriz" 							=> 0
						] ;

		$array_sp = array();

		foreach ($resultado as $variable) 
		{
			$array_sp[] = $variable;
			// $fecha =  date("M-y",strtotime($variable["fecha"]));
			// $seguro = utf8_encode($variable["descripcion_seguro"]);
			// $valor =  $variable["total"];

			// if($valor==0 || $valor==null)
			// {
			// 	$array_def[$fecha][$seguro] = 0;
			// 	$array_dif[$seguro][$fecha] = 0;
			// }
			// else
			// {
			// 	$array_def[$fecha][$seguro] = (int)$variable["total"];
			// 	$array_dif[$seguro][$fecha] = (int)$variable["total"];
			// }
		}
		mysqli_free_result($resultado);

		

	// 	// $array_fechas = array_fill_keys(array_keys($array_def), 0);
	// 	// $array_seguros = array_fill_keys(array_keys($array_dif), 0);

	// 	// foreach ($array_def as $lm => $valora)
	// 	// {
	// 	// 	$array_def[$lm] = $valora + $array_seguros;
	// 	// }

	// 	// foreach ($array_dif as $ll => $valor)
	// 	// {
	// 	// 	$array_dif[$ll] = $valor + $array_fechas;
	// 	// }
		
	// 	// $table = $to;
	// 	// $table .= $theado;
	// 	// $table .= $tro;
	// 	// $table .= $tho."Fecha".$thc;
	// 	// foreach ($array_seguros as $key => $value) 
	// 	// {
	// 	// 	$table .= $tho.$key.$thc;
	// 	// }
	// 	// $table .= $trc;
	// 	// $table .= $theadc;
		
	// 	// $table .= $tbo;
	// 	// foreach ($array_def as $fecha => $seguro) 
	// 	// {
	// 	// 	$table .= $tro;
	// 	// 	$table .= $tdo.$fecha.$tdc;
	// 	// 	foreach ($seguro as $sk => $valore) 
	// 	// 	{
	// 	// 		$table .= $tdo.$valore.$tdc;
	// 	// 	}
	// 	// 	$table .= $trc;
	// 	// }

	// 	// $table .= $tbc;
	// 	// $table .= $tc;
	// 	// echo $table;



	// 	// $table2 = $to;
	// 	// $table2 .= $theado;
	// 	// $table2 .= $tro;
	// 	// $table2 .= $tho."Seguro".$thc;
	// 	// foreach ($array_fechas as $key => $value) 
	// 	// {
	// 	// 	$table2 .= $tho.$key.$thc;
	// 	// }
	// 	// $table2 .= $trc;
	// 	// $table2 .= $theadc;
	// 	// $table2 .= $tbo;
	// 	// foreach ($array_dif as $key => $value) 
	// 	// {
	// 	// 	$table2 .= $tro;
	// 	// 	$table2 .= $tdo.$key.$tdc;
	// 	// 	foreach ($value as $ky => $vl) 
	// 	// 	{
	// 	// 		$table2 .= $tdo.$vl.$tdc;
	// 	// 	}
	// 	// 	$table2 .= $trc;
	// 	// }
	// 	// $table2 .= $tbc;
	// 	// $table2 .= $tc;
	// 	// echo $table2;

	}

	$conn->close();
	$array_totales = array();

	foreach ($array_sp as $vlaw) 
	{
		$fecha =  date("M-y",strtotime($vlaw["fecha"]));
		$seguro = trim(utf8_encode($vlaw["descripcion_seguro"]));
		
		$valor =  $vlaw["total"];

		if($valor==0 || $valor==null)
		{
			$array_def[$fecha][$seguro] = 0;
			$array_dif[$seguro][$fecha] = 0;
		}
		else
		{
			$array_def[$fecha][$seguro] = (int)$vlaw["total"];
			$array_dif[$seguro][$fecha] = (int)$vlaw["total"];
		}
	}

	$table_fragments = array();
	$table_fragments= [
		"tO" => "<table class=\"table table-dark\">",
		"tC" => "</table>",
		"theadO" =>"<thead>",
		"theadC" =>"</thead>",
		"thO" =>"<th>",
		"thC" =>"</th>",
		"trO" =>"<tr>",
		"trC" =>"</tr>",
		"tbO" =>"<tbody>",
		"tbC" =>"</tbody>",
		"tdO" =>"<td>",
		"tdC" =>"</td>"
	];

	$array_fechas  = array_fill_keys(array_keys($array_def), 0);
	$array_seguros = array_fill_keys(array_keys($array_dif), 0);

	foreach ($array_def as $lm => $valora)
	{
		$array_def[$lm] = array_merge($array_seguros,$valora);
	}

	foreach ($array_dif as $ll => $valor)
	{
		$array_dif[$ll] = $valor + $array_fechas;
	}

	
	
	function getDataTableResumen()
	{
		$servername = "localhost";
		$username = "root";
		$password = "";
		$database = "seguros";

		$conn = new mysqli($servername, $username, $password,$database);

		

		if ($conn->connect_error) {
		    die("Connection failed: " . $conn->connect_error);
		} 

		$array_resumen = array();
		$sql = "SELECT
					ResumenVentaDescTipoProducto as DescTipoProducto,
				    ResumenVentaDescSubProducto	as DescSubProducto,
				    ResumenVentaNombre as Nombre,
				    ResumenVentaPrima as Prima,
				    ResumenVentaFecVigenciaInicialOperacion as Fecha,
				    ResumenVentaRut as Rut,
				    ResumenVentaTipoAsegurado as TipoAsegurado,
				    ResumenVentaCanalDeInicio as CanalDeInicio,
				    ResumenVentaCanalDeTermino as CanalDeTermino,
				    IFNULL(ResumenVentaRutEjecutivo,'') as RutEjecutivo
					from seguros.ResumenVenta";


		if($resultado = $conn->query($sql))
		{
			foreach($resultado as $variable) 
			{
				$array_resumen[] = $variable;
			}
		}
		mysqli_free_result($resultado);
		$conn->close();
		return $array_resumen;
	}
	/********************************************************************************************
	********************************************************************************************/

	$array_resumen = getDataTableResumen();
	tabla3dias($table_fragments);

	function tabla3dias($t_help)
	{
		$servername = "localhost";
		$username = "root";
		$password = "";
		$database = "seguros";

		$conn = new mysqli($servername, $username, $password,$database);

		

		if ($conn->connect_error) {
		    die("Connection failed: " . $conn->connect_error);
		} 

		$array_resumen = array();
		$sql = "CALL `seguros`.`spVentasPorSeguro3dias`()";



		$arrkx = array();
		$arrm = array();
		$arrn = array("menos3"=>array());
		if($resultado = $conn->query($sql))
		{
			$info = $resultado->fetch_fields();
			foreach($info as $varsx) 
			{
				$arrkx[] = utf8_encode($varsx->name);
				//echo $varsx->name;
				//mysqli_fetch_array()
				//echo $resultado[$variable];
				//$arrx[] = $resultado[$key];
			}
			$arrkx[] = "hastaEsteDia";
			$arrm = array_fill_keys($arrkx, array());

			$arwea = array();
			$arrsemi = array();

			//var_dump($arrm);
			$offset = 3;

			// while($fila = $resultado->fetch_assoc())
			// {
			// 	echo "<pre>";
			// 	echo var_dump($fila);
			// 	echo "</pre>";
			// }

			foreach ($resultado as $llave => $value)
			{
				// echo "<pre>";
				// var_dump($resultado);
				// echo "</pre>";
				

				//echo utf8_encode(var_dump($value));
				foreach ($arrm as $key => $valo)
				{
					//echo utf8_encode($value);
					//echo $key.": ";
					// echo $key."<br>";
					// echo $key."<br>";
					// echo $key;
					//echo $value;
					if(isset($value[utf8_decode($key)]))
					{
						// echo $key;
						// echo "aqui"."<br>";
						$arrm[$key][] = $value[utf8_decode($key)];
						//echo $value[utf8_decode($key)]."\n";
					}
					else
					{
						// echo $key;
						// echo "aquo"."<br>";
						$arrm[$key][] = 0;
					}
					
					
					//$valor = $value[$key];
				}
			}


			


			// echo "<pre>";
			// var_dump($arrm);
			// echo "</pre>";
			mysqli_free_result($resultado);
		}



		// echo "<pre>";
		// echo var_dump($arrkx);
		// echo "</pre>";
		
		$conn->next_result(); 
		$sql = "CALL `seguros`.`spVentasTotales3dias`()";
	//	$arrn["menos3"] = 3;
	//	
		

		if($r = $conn->query($sql))
		{
			foreach ($r as $key => $value)
		 	{
		 		//echo $r[$key];
				$arrn["menos3"][] =  $value["menos3"];
			}
		}

		$arrm["hastaEsteDia"] = $arrn["menos3"];

		// echo "<pre>";
		// var_dump($arrm);
		// echo "</pre>";

		// foreach ($arrm as $key => $value) 
		// {
			
		// }

		
		echo "<div class=\"col-12\">";
		echo "<div class=\"row\">";
		for ($i=0; $i < 3; $i++)
		{ 
			
			$tb_res   = "<div class=\"col\">";
			//$tb_res   = $t_help["tO"];
			$tb_res  .= "<table id=\"\" class=\"table table-dark\">";
			//$tb_res  .= $t_help["theadO"];
			// $tb_res  .= $t_help["trO"];

			// foreach ($arrm as $key => $value) 
			// {
			// 	if($value[$i]!=0)
			// 	{
			// 		$tb_res  .= $t_help["thO"].$key.$t_help["thC"];	
			// 	}
			// }

			// $tb_res  .= $t_help["trC"];
			// $tb_res  .= $t_help["theadC"];
			$tb_res  .= $t_help["tbO"];
			// for($oi = 0; $oi < count($oi); $oi++ )
			// {
			// 	//$offset = 0;
			
			foreach ($arrm as $key => $value)
			{
				if($value[$i]!=0)
				{
					$tb_res  .= $t_help["trO"];
					$tb_res  .= "<td class=\"\">".$key.$t_help["tdC"];	
					$tb_res  .= "<td class=\"text-right\">".$value[$i].$t_help["tdC"];	
					$tb_res  .= $t_help["trC"];
				}
				//$tb_res .= $t_help["tdO"].$value[$i].$t_help["tdC"];
				//echo $value[$i];
				//echo "<br>";
				// foreach ($value as $key => $vax) 
				// {
				// 	//echo $key;
				// 	break;
				// }
			}
			
			$tb_res  .= $t_help["tbC"];
			$tb_res  .= $t_help["tC"];
			$tb_res  .= "</div>";
			echo $tb_res;
				
			// }
		}
		echo "</div>";
		echo "</div>";

		


		// foreach ($arrm as $value) 
		// {
		// 	// echo "<pre>";
		// 	// echo count($arrm);
		// 	// echo "</pre>";
		// 	$tb_res  .= $t_help["trO"];
		// 	foreach ($value as $key => $valores)
		// 	{
		// 		$tb_res .= $t_help["tdO"].utf8_encode($valores).$t_help["tdC"];
		// 	}	
		// 	$tb_res  .= $t_help["trC"];
		// }
		// $tb_res  .= $t_help["tbC"];
		// $tb_res  .= $t_help["tC"];

		

		// foreach ($variable as $value) 
		// {
			
		// }
		// echo "<pre>";
		// echo var_dump($arrm);
		// echo "</pre>";
		//mysqli_free_result($r);


		$conn->close();
		return $array_resumen;
	}

	// echo "<pre>";
	// 	echo var_dump(tabla_resumen($array_resumen,$array_seg,$table_fragments));
	// echo "</pre>";

	function tabla_resumen($array_resumen,$arr_help,$arr_tbl)
	{

		$array_filtrado = array();
		$offset = 0;

		$array_ftemp = array();
		foreach ($array_resumen as $key => $value) 
		{
			$array_ftemp[] = $array_resumen[$key]["Fecha"];
		}

		
		for($xxa = 0 ; $xxa < 3 ; $xxa++)
		{

			$fec_max = date_create(max($array_ftemp));
			$dd = ($xxa)." days";
			date_sub($fec_max,date_interval_create_from_date_string($dd));

			//$fecha_maxima,date_interval_format()

			$year = date_format($fec_max,"Y");
			$month = date_format($fec_max,"n");
			$fec_min = date_create("$year-$month-01");

			//echo "hola:".array_search($fecha_minima,$array_resumen);
			
			echo "offset: ".$offset;
			$array_filtrado[$offset] = array_fill_keys(array_keys($arr_help), 0);
			echo var_dump($array_filtrado);
			foreach ($array_resumen as $key => $value) 
			{
				$fecT = date_create($value["Fecha"]);
				if($fecT>=$fec_min && $fecT<=$fec_max)
				{
					echo utf8_decode($array_resumen[$key]["DescSubProducto"]);
					switch (trim(utf8_encode($array_resumen[$key]["DescSubProducto"]))) 
					{
						case "Vida Con Devolución Clásico Internet":
							$array_filtrado[$offset]["Vida Con Devolución Clásico Internet"]+=1;
						break;

						case "Proteccion Tradicional Full Internet":
							$array_filtrado[$offset]["Proteccion Tradicional Full Internet"]+=1;
						break;

						case "Proteccion Preferente Full Internet":
							$array_filtrado[$offset]["Proteccion Preferente Full Internet"]+=1;
						break;

						case "Proteccion Tradicional":
							$array_filtrado[$offset]["Proteccion Tradicional"]+=1;
						break;

						case "Proteccion Integral Tarjeta":
							$array_filtrado[$offset]["Proteccion Integral Tarjeta"]+=1;
						break;

						case "Hogar Contenido":
							$array_filtrado[$offset]["Hogar Contenido"]+=1;
						break;

						case "Renta Hospitalaria":
							$array_filtrado[$offset]["Renta Hospitalaria"]+=1;
						break;

						case "Bike":
							$array_filtrado[$offset]["Bike"]+=1;
						break;

						case "Vida Digital":
							$array_filtrado[$offset]["Vida Digital"]+=1;
						break;

						case "Emergencia Accidental":
							$array_filtrado[$offset]["Emergencia Accidental"]+=1;
						break;

						case "Mascota":
							$array_filtrado[$offset]["Mascota"]+=1;
						break;

						case "Seguro Cardiologico Internet":
							$array_filtrado[$offset]["Seguro Cardiologico Internet"]+=1;
						break;

						case "Viaje Protegido Plus":
							$array_filtrado[$offset]["Viaje Protegido Plus"]+=1;
						break;

						case "Egra":
							$array_filtrado[$offset]["Egra"]+=1;
						break;

						case "Linea Protegida":
							$array_filtrado[$offset]["Linea Protegida"]+=1;
						break;

						case "Vida Educacion":
							$array_filtrado[$offset]["Vida Educacion"]+=1;
						break;

						case "Asistencia Premium":
							$array_filtrado[$offset]["Asistencia Premium"]+=1;
						break;

						case "Oncologico":
							$array_filtrado[$offset]["Oncologico"]+=1;
						break;

						case "Automotriz":
							$array_filtrado[$offset]["Automotriz"]+=1;
						break;
					}
					//array_push($array_filtrado[$offset], $array_resumen[$key]);
				}
			}
			$offset++;


			
			// $fecha_maxima[] = $fec_max;
			// $fecha_minima[] = $fec_min;




			echo date_format($fec_min, "Y-m-d");
			//echo $fecha_maxima;
			echo date_format($fec_max,"Y-m-d");
			//echo date_format($fecha_minima,"Y-m-d");
			//$offset = 3
		}

		$vcd 	= 0; 	//Vida Con Devolución x
		$pit 	= 0;	//Protección Integral Tarjeta //
		$pt 	= 0; 	//Protección Tradicional //
		$ptfi 	= 0;	//Protección Tradicional Internet Full x
		$ppfi 	= 0;	//Protección Preferente Full Internet x
		$hc		= 0;	//Hogar Contenido x 
		$rh 	= 0;	//Renta Hospitalaria //
		$bk 	= 0;	//Bike //
		$vd 	= 0;	//Vida Digital //
		$ea 	= 0;	//Emergencia Accidental //
		$mst 	= 0;	//Mascotas //
		$crd 	= 0;	//Cardiológico x
		$vp 	= 0;	//Viaje Protegido x
		$ei 	= 0;	//Egra Internet x
		$lp 	= 0;	//Linea Protegida //
		$ap 	= 0;	//Asistencia Premiun //
		$ve 	= 0;	//Vida Educacion //
		$onc 	= 0;	//Oncologico //
		$atm 	= 0;	//Automotriz x
		// for ($i=3; $i >= 1 ; $i--) 
		// { 

		// 	$array_filtrado




		// }

		return $array_filtrado;
	}


	/**************************************************************************************************
	**************************************************************************************************/

	$c_per_page = 100;
	$current_page = 1;
	if(isset($_GET["nPag"]))
	{
		$current_page = $_GET["nPag"];
	}
	else
	{
		$current_page = 1;
	}
	$total_pages = ceil(count($array_resumen)/$c_per_page);
	$total_records = count($array_resumen);

	$arr_pag_list = array("c_per_page"=>$c_per_page
		,"current_page" => $current_page
		,"total_pages"=>$total_pages
		,"total_records"=>$total_records);

	//echo $total_pages;

	//echo $current_page;
?>
<ul class="pagination pagination-lg justify-content-center">


	<li class="page-item"><a class="page-link" href="?nPag=1" >Primera</a></li>
	<li class="<?php if($current_page<=1) { echo 'disabled'; } ?> page-item"
		style="display:<?php if($current_page<=1 || $current_page<=2) { echo 'none'; } ?>"
	 >
		<a class="page-link" href="<?php echo ($current_page <=1) ? '#' : ("?nPag=".($current_page-1)) ?>">
			<?php echo ($current_page <=1) ? 1 : (($current_page-1)) ?>
				
			</a>
	</li>
	<li class="page-item active ">
		<a class="page-link " href="#"> <?php echo ($current_page) ?> </a>
	</li>
	<li class="<?php if($current_page>=$total_pages){ echo 'disabled'; }?> page-item">
		<a class="page-link" href="<?php echo ($current_page >=$total_pages) ? '#' : ("?nPag=".($current_page+1)) ?>">
			<?php echo ($current_page >=$total_pages) ? $total_pages : (($current_page+1)) ?></a>
	</li>
	<li class="page-item"><a class="page-link" href="?nPag=<?php echo $total_pages;?>">Ultima</a></li>
</ul>

<?php

	//createTableResumen($array_resumen,$table_fragments,$arr_pag_list);
	createTableResumen1($array_def,$array_seguros,$table_fragments);
	//createTableResumen2($array_dif,$array_fechas,$table_fragments);
	
	

	function createTableResumen($array_resumen,$t_help,$arr_pag_list)
	{
		// $servername = "localhost";
		// $username = "root";
		// $password = "";
		// $database = "seguros";

		// $conn = new mysqli($servername, $username, $password,$database);

		

		// if ($conn->connect_error) {
		//     die("Connection failed: " . $conn->connect_error);
		// } 

		// $array_resumen = array();
		// $sql = "SELECT
		// 			ResumenVentaDescTipoProducto as DescTipoProducto,
		// 		    ResumenVentaDescSubProducto	as DescSubProducto,
		// 		    ResumenVentaNombre as Nombre,
		// 		    ResumenVentaPrima as Prima,
		// 		    ResumenVentaFecVigenciaInicialOperacion as Fecha,
		// 		    ResumenVentaRut as Rut,
		// 		    ResumenVentaTipoAsegurado as TipoAsegurado,
		// 		    ResumenVentaCanalDeInicio as CanalDeInicio,
		// 		    ResumenVentaCanalDeTermino as CanalDeTermino,
		// 		    IFNULL(ResumenVentaRutEjecutivo,'') as RutEjecutivo
		// 			from seguros.ResumenVenta";

		// if($resultado = $conn->query($sql))
		// {
		// 	foreach($resultado as $variable) 
		// 	{
		// 		$array_resumen[] = $variable;
		// 	}
		// }

		// $conn->close();

		//$array_headers = array_keys($array_def)
		$tb_res   = "";
		$tb_res  .= "<table id=\"tabla_resumen\" class=\"table table-dark\">";

		$tb_res  .= $t_help["theadO"];
		$tb_res  .= $t_help["trO"];

		$tb_res  .= $t_help["thO"]."DescTipoProducto".$t_help["thC"];
		$tb_res  .= $t_help["thO"]."DescSubProducto".$t_help["thC"];
		$tb_res  .= $t_help["thO"]."Nombre".$t_help["thC"];
		$tb_res  .= $t_help["thO"]."PrimaTotal".$t_help["thC"];
		$tb_res  .= $t_help["thO"]."Fecvigenciainicialoperacion".$t_help["thC"];
		$tb_res  .= $t_help["thO"]."RUT".$t_help["thC"];
		$tb_res  .= $t_help["thO"]."TipoAsegurado".$t_help["thC"];
		$tb_res  .= $t_help["thO"]."CanaldeInicio".$t_help["thC"];
		$tb_res  .= $t_help["thO"]."CanaldeTermino".$t_help["thC"];
		$tb_res  .= $t_help["thO"]."RUTEJECUTIVO".$t_help["thC"];

		$tb_res  .= $t_help["trC"];
		$tb_res  .= $t_help["theadC"];

		$tb_res  .= $t_help["tbO"];


		$calc_max_rows = $arr_pag_list["current_page"] * 100 < $arr_pag_list["total_records"] ? 
		$arr_pag_list["current_page"] * 100 : $arr_pag_list["total_records"];
		$curr_page = ($arr_pag_list["current_page"]-1) * 100 ;
		//echo $calc_max_rows;
		for ($i=$curr_page; $i < $calc_max_rows ; $i++) 
		{
			$tb_res  .= $t_help["trO"];
				// for ($i=1; $i < count($arr_value); $i++) 
				// { 
				// 	$tb_res .= $t_help["tdO"].$arr_value[$i].$t_help["tdC"];
				// }
				foreach ($array_resumen[$i] as $value)
				{
					$tb_res .= $t_help["tdO"].utf8_encode($value).$t_help["tdC"];
				}
				$tb_res  .= $t_help["trC"];
		
			// foreach ($array_resumen as $arr_value) 
			// {
			// 	$tb_res  .= $t_help["trO"];
			// 	// for ($i=1; $i < count($arr_value); $i++) 
			// 	// { 
			// 	// 	$tb_res .= $t_help["tdO"].$arr_value[$i].$t_help["tdC"];
			// 	// }
			// 	foreach ($arr_value as $value)
			// 	{
			// 		$tb_res .= $t_help["tdO"].utf8_encode($value).$t_help["tdC"];
			// 	}
			// 	$tb_res  .= $t_help["trC"];
			// }
		}

		$tb_res  .= $t_help["tbC"];

		$tb_res  .= $t_help["tC"];

		echo $tb_res;

		echo "<div>";

		echo "<a></a>";

		echo "</div>";

		
	}

	function createTableResumen1($arr_tabla1,$arr_helper,$t_help)
	{

		//$arr_temp = ;
		$txr1   = $t_help["tO"];
		$txr1  .= $t_help["theadO"];
		$txr1  .= $t_help["trO"];

		foreach ($arr_tabla1 as $ar_ob1 => $vvv)
		{
			foreach ($arr_helper as $ar_ob2 => $vol) 
			{
				if(array_key_exists($ar_ob2, $vvv))
				{
					$arr_helper[$ar_ob2] += $vvv[$ar_ob2];
				}
			}
		}
			
		// echo "<pre>";
		// var_dump($arr_tabla1);
		// echo "</pre>";

		foreach ($arr_helper as $arh_ob => $vol) 
		{
			$txr1 .= $t_help["thO"].$arh_ob.$t_help["thC"];
		}
		$txr1 .= $t_help["thO"]."Total".$t_help["thC"];
		$txr1 .= $t_help["trC"];
		$txr1 .= $t_help["theadC"];

		$txr1 .= $t_help["tbO"];
		$txr1 .= $t_help["trO"];
		$contador = 0;

		foreach ($arr_helper as $arh_ob => $vol)
		{
			$contador +=$vol;
			$txr1 .= $t_help["tdO"].$vol.$t_help["tdC"];
		}
		$txr1 .= $t_help["tdO"].$contador.$t_help["tdC"];
		$txr1 .= $t_help["trC"];
		$txr1 .= $t_help["tbC"];
		$txr1 .= $t_help["tC"];

		echo $txr1;

		$table  = $t_help["tO"];
		$table .= $t_help["theadO"];
		$table .= $t_help["trO"];
		$table .= $t_help["thO"]."Fecha".$t_help["thC"];
		foreach ($arr_helper as $key => $value) 
		{
			$table .= $t_help["thO"].$key.$t_help["thC"];
		}

		$table .= $t_help["thO"]."Total".$t_help["thC"];
		$table .= $t_help["trC"];
		$table .= $t_help["theadC"];
		
		$table .= $t_help["tbO"];

		$cuentaT = 0;
		foreach ($arr_tabla1 as $fecha => $seguro) 
		{
			$cuentaTMY = 0;
			$table .= $t_help["trO"];
			$table .= $t_help["tdO"].$fecha.$t_help["tdC"];
			foreach ($seguro as $sk => $valore) 
			{
				$table .= $t_help["tdO"].$valore.$t_help["tdC"];
				// $cuentaTMY+=$valore;
				$cuentaTMY+=$valore;
			}
			$table .= $t_help["tdO"].$cuentaTMY.$t_help["tdC"];
			$table .= $t_help["trC"];
		}

		$table .= $t_help["tbC"];
		$table .= $t_help["tC"];
		echo $table;
		echo $cuentaT;
	}

	function createTableResumen2($arr_tabla2,$arr_helper,$t_help)
	{
		$table2  = $t_help["tO"];
		$table2 .= $t_help["theadO"];
		$table2 .= $t_help["trO"];
		$table2 .= $t_help["thO"]."Seguro".$t_help["thC"];
		foreach ($arr_helper as $key => $value) 
		{
		
			$table2 .= $t_help["thO"].$key.$t_help["thC"];
		}
		$table2 .= $t_help["trC"];
		$table2 .= $t_help["theadC"];
		$table2 .= $t_help["tbO"];
		foreach ($arr_tabla2 as $key => $value) 
		{
			$table2 .= $t_help["trO"];
			//$table2 .= $tdo.$key.$tdc;
			$table2 .= $t_help["tdO"].$key.$t_help["tdC"];
			foreach ($value as $ky => $vl) 
			{
				$table2 .= $t_help["tdO"].$vl.$t_help["tdC"];
				//$table2 .= $tdo.$vl.$tdc;
			}
			$table2 .= $t_help["trC"];
		}
		$table2 .= $t_help["tbC"];
		$table2 .= $t_help["tC"];
		echo $table2;
	}
?>


<script>

	$('#tabla_resumen').DataTable( {
		paging:true
		
} );
</script>

</div>
</body>
</html>