CREATE TABLE `resumenventa` (
  `ResumenVentaId` int(11) NOT NULL AUTO_INCREMENT,
  `ResumenVentaDescTipoProducto` varchar(255) DEFAULT NULL,
  `ResumenVentaDescSubProducto` varchar(255) DEFAULT NULL,
  `ResumenVentaNombre` varchar(500) DEFAULT NULL,
  `ResumenVentaPrima` float DEFAULT NULL,
  `ResumenVentaFecVigenciaInicialOperacion` date DEFAULT NULL,
  `ResumenVentaRut` varchar(255) DEFAULT NULL,
  `ResumenVentaTipoAsegurado` varchar(255) DEFAULT NULL,
  `ResumenVentaCanalDeInicio` varchar(255) DEFAULT NULL,
  `ResumenVentaCanalDeTermino` varchar(255) DEFAULT NULL,
  `ResumenVentaRutEjecutivo` varchar(255) DEFAULT NULL,
  `ResumenVentaPlan` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`ResumenVentaId`)
) ENGINE=MyISAM AUTO_INCREMENT=51 DEFAULT CHARSET=latin1;