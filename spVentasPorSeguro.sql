DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `spVentasPorSeguro`()
BEGIN
SELECT concat(year(ResumenVentaFecVigenciaInicialOperacion),'-',month(ResumenVentaFecVigenciaInicialOperacion))
					as fecha,
					COUNT(ResumenVentaDescSubProducto) as total,
			CASE  ResumenVentaDescSubProducto
				when 'Incendio Y Robo Internet' THEN 
					replace(ResumenVentaDescSubProducto,'Incendio Y Robo Internet','Hogar Contenido')
				when 'Viaje Protegido Plus' THEN 
					replace(ResumenVentaDescSubProducto,'Viaje Protegido Plus','Viaje Protegido')
				when 'Vida Con Devolución Clásico Internet 1' THEN 
					replace(ResumenVentaDescSubProducto,'Vida Con Devolución Clásico Internet 1','Vida Con Devolución Clásico Internet')
				else ResumenVentaDescSubProducto
			END
			as descripcion_seguro
				from ResumenVenta
			group by
            fecha,
			descripcion_seguro
			order by
            ResumenVentaFecVigenciaInicialOperacion desc,
		    descripcion_seguro desc;
END$$
DELIMITER ;
