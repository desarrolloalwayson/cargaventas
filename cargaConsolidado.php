<?php
	require 'vendor/autoload.php';
	const MIN_DATES_DIFF = 25569;
	const SEC_IN_DAY = 86400;
	/*
		CREDENCIALES DB
	 */
	$servername = "localhost";
	$username = "root";
	$password = "";
	$database = "seguros";

	$conn = new mysqli($servername, $username, $password,$database);

	if ($conn->connect_error) {
	    die("Connection failed: " . $conn->connect_error);
	}

	$sql = "TRUNCATE TABLE resumenVenta";
	$conn->query($sql);

	
	$stmt = $conn->prepare("INSERT INTO resumenVenta 
								(
									resumenVentaDescTipoProducto,
									resumenVentaDescSubProducto,
									resumenVentaNombre,
									resumenVentaPrima,
									resumenVentaFecVigenciaInicialOperacion,
									resumenVentaRut,
									resumenVentaTipoAsegurado,
									resumenVentaCanalDeInicio,
									resumenVentaCanalDeTermino,
									resumenVentaRutEjecutivo
								)	
								VALUES
								(
									?,?,?,?,?,?,?,?,?,?
								)");
	

	$nombre_Consolidado = './CONSOLIDADO/DICIEMBRE.xlsx';
	$nombrePagina = 'Detalle de ventas';

	$tipoArchivo = \PhpOffice\PhpSpreadsheet\IOFactory::identify($nombre_Consolidado);

	$lector = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($tipoArchivo);
	$lector->setLoadSheetsOnly($nombrePagina);

	$planilla = $lector->load($nombre_Consolidado);
	
	$hoja = $planilla->getActiveSheet();

	$maxRowIndex = $hoja->getHighestRow();

	$maxCol = $hoja->getHighestColumn();
	$maxColIndex = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString($maxCol);

	echo $maxRowIndex;
	$contador = 0;
	try {
	for ($fila=2; $fila <= $maxRowIndex; $fila++) 
	{ 
			
			$resumen_DescTipoProducto =trim($hoja->getCellByColumnAndRow(1,$fila)->getValue());
			$resumen_DescSubProducto = checkDescSeguro(utf8_decode(ucwords(mb_strtolower($hoja->getCellByColumnAndRow(2,$fila)->getValue()))));
			$resumen_Nombre = ucwords(strtolower(utf8_decode($hoja->getCellByColumnAndRow(3,$fila)->getValue())));
			$resumen_Prima = $valores =  str_replace(',','.',$hoja->getCellByColumnAndRow(4,$fila)->getValue());
			$resumen_FecVigenciaInicialOperacion = excel2date($hoja->getCellByColumnAndRow(5,$fila)->getValue());
			$resumen_Rut = $hoja->getCellByColumnAndRow(6,$fila)->getValue();
			$resumen_TipoAsegurado =ucwords(strtolower ($hoja->getCellByColumnAndRow(7,$fila)->getValue()));
			$resumen_CanalDeInicio =ucwords(strtolower($hoja->getCellByColumnAndRow(8,$fila)->getValue()));
			$resumen_CanalDeTermino =ucwords(strtolower ($hoja->getCellByColumnAndRow(9,$fila)->getValue()));
			$resumen_RutEjecutivo =$hoja->getCellByColumnAndRow(10,$fila)->getValue();

			
			$stmt->bind_param("sssdssssss",
				$resumen_DescTipoProducto,
				$resumen_DescSubProducto,
				$resumen_Nombre,
				$resumen_Prima,
				$resumen_FecVigenciaInicialOperacion,
				$resumen_Rut,
				$resumen_TipoAsegurado,
				$resumen_CanalDeInicio,
				$resumen_CanalDeTermino,
				$resumen_RutEjecutivo
			);
			$stmt->execute();
			echo "<pre>";
			// if($stmt->sqlstate != '00000')
			// {
			 	//echo "prima: ".$hoja->getCellByColumnAndRow(4,$fila)->getValue()." ";
			 	
				echo("objeto: $contador  " . $stmt->error);
			// }
			echo "</pre>";
			// echo '<td>' . 
			// 	$hoja->getCellByColumnAndRow($columna,$fila)->getValue();
		 // 	'</td>' . PHP_EOL;
		$contador++;
		
	}}catch(Exception $e)
	{
		echo $e->getMessage();
	}
	$stmt->close();
	$conn->close();


	/**
	 * [checkDescSeguro Chequea la descripcion en busca de inconsistencias]
	 * @return [string] [retorna la descripcion en el formato deseado]
	 */
	//function checkDescSeguro($strDesc,$fecha)
	function checkDescSeguro($strDesc)
	{
		
		$desc = "";
		$desc = $strDesc;
		//$fec = date($fecha);

		//$date_start_egra = date_create('2016-07-31');

		if(stripos($desc, "VIDA CON DEVOLUCIÓN CLÁSICO INTERNET")!==false)
		{	
			$desc = ucwords(strtolower(utf8_decode("vida con devolución clásico internet")));
			echo $desc;
		}

		//if(strpos($desc, "EGRA")!==false && $fec > $date_start_egra )
		if(strpos($desc, "EGRA")!==false)
		{	
			$desc = ucwords(strtolower(("EGRA")));
		}

		if(stripos($desc, "INCENDIO Y ROBO INTERNET")!==false)
		{	
			$desc = ucwords(strtolower("HOGAR CONTENIDO"));
		}

		return $desc;
	}

	/*
		funcion para convertir timestamps excel a timestamps unix 
		(cuidado con el offset unix no lo posee osea es zulu time , GMT +0)
		
		-------------------------sacado de aqui----------------------------------
		http://fczaja.blogspot.com/2011/06/convert-excel-date-into-timestamp.html
		-------------------------------------------------------------------------

		esta es una version Modificada
	*/
	

	function excel2date($excelDate)
	{

		// define("MIN_DATES_DIFF", 25569);
		// define("SEC_IN_DAY", 86400); 
	   $timeStampU = 0;
	   if (!($excelDate <= MIN_DATES_DIFF))
	   {
	     $timeStampU = ($excelDate - MIN_DATES_DIFF) * SEC_IN_DAY;
	   }

	   $fmtDate = gmdate("Y-m-d",$timeStampU);

	   return $fmtDate;
	}
	/**/
?>