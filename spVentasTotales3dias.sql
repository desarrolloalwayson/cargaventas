DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `spVentasTotales3dias`()
BEGIN
SELECT COUNT(*) as menos3 FROM resumenventa WHERE ResumenVentaFecVigenciaInicialOperacion BETWEEN
date_format((SELECT max(rrrr.ResumenVentaFecVigenciaInicialOperacion) FROM resumenventa rrrr), '%Y-%m-01')
AND cast((SELECT date_sub(max(ResumenVentaFecVigenciaInicialOperacion) ,INTERVAL 2 DAY) FROM resumenventa )AS DATE)
union
SELECT COUNT(*) AS menos3 FROM resumenventa WHERE ResumenVentaFecVigenciaInicialOperacion BETWEEN
date_format((SELECT max(rrrr.ResumenVentaFecVigenciaInicialOperacion) FROM resumenventa rrrr), '%Y-%m-01')
AND cast((SELECT date_sub(max(ResumenVentaFecVigenciaInicialOperacion) ,INTERVAL 1 DAY) FROM resumenventa )AS DATE)
UNION
SELECT COUNT(*) AS menos3 FROM resumenventa WHERE ResumenVentaFecVigenciaInicialOperacion BETWEEN
date_format((SELECT max(rrrr.ResumenVentaFecVigenciaInicialOperacion) FROM resumenventa rrrr), '%Y-%m-01')
AND cast((SELECT max(ResumenVentaFecVigenciaInicialOperacion) FROM resumenventa )AS DATE);
END$$
DELIMITER ;